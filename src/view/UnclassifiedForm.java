package view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import tools.UpdateMessage;
import controller.Controller;
import controller.Observer;
import datastructure.Anime;
import datastructure.AnimeEntry;

public class UnclassifiedForm extends JPanel implements Observer{
	private static final long serialVersionUID = 1L;
	private Controller controller;
	private ListPanel listPanel;
	private AnimeForm rightPanel;

	public UnclassifiedForm(Controller controller) {

		this.controller = controller;
		this.controller.addObserver(this);
		

		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 0.5;
		gbc.weighty = 1;
		gbc.insets = new Insets(10, 10, 10, 20);
		gbc.anchor = GridBagConstraints.FIRST_LINE_START;
		this.add(initLeftPanel(), gbc);
		rightPanel = new AnimeForm(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				AnimeEntry episode = UnclassifiedForm.this.rightPanel.getAnimeEntry();
				String animeName = UnclassifiedForm.this.rightPanel.getAnime();
				String genre = UnclassifiedForm.this.rightPanel.getGenre();
				String location = UnclassifiedForm.this.rightPanel.getFolderLocation();
				int season = UnclassifiedForm.this.rightPanel.getSeason();
				episode.setSeason(season);
				UnclassifiedForm.this.controller.registerAnime(episode, new Anime(animeName, genre, location));
			}
		});

		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.weightx = 0.5;
		gbc.weighty = 1;
		gbc.insets = new Insets(10, 20, 10, 10);
		gbc.anchor = GridBagConstraints.FIRST_LINE_START;
		this.rightPanel.updateInfos(new ArrayList<Anime>());
		this.add(rightPanel, gbc);
	}

	public JPanel initLeftPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		JLabel label = new JLabel("Liste des �pisodes � trier");
		panel.add(label);

		listPanel = new ListPanel(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting())
				{
					AnimeEntry entry = UnclassifiedForm.this.listPanel.getSelected();
					if (entry != null)
						UnclassifiedForm.this.rightPanel.setAnime(entry);
				}
			}
		});
		panel.add(listPanel);

		JButton btnUpdate = new JButton("Scanner");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.scan();
			}
		});
		panel.add(btnUpdate);

		return panel;
	}

	public void update(UpdateMessage message) {
		switch (message.getState()) {
		case INIT:
		case NEWANIME:
			ArrayList<Anime> listAnimes = new ArrayList<Anime>();
			listAnimes = controller.getListAnimeKnown();
			this.rightPanel.updateInfos(listAnimes);
			break;
		case SCAN:
			ArrayList<AnimeEntry> list = controller.getListAnimeUnresolved();
			listPanel.addElements(list);
			break;
		}
		repaint();
	}

}
