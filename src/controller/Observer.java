package controller;

import tools.UpdateMessage;

public interface Observer {
	public void update(UpdateMessage l);
}
