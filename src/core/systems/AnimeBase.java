package core.systems;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;

import datastructure.*;

public class AnimeBase
{
    private Connection connection = null;
    private Statement  statement  = null;

    public void initialise(String url) throws ClassNotFoundException,
	    SQLException
    {
	Class.forName("org.sqlite.JDBC");
	connection = DriverManager.getConnection(url);
	statement = connection.createStatement();

	try
	{
	    String request = new String(Files.readAllBytes(Paths
		    .get("Anime.sql")));
	    // System.out.println(request);
	    statement.executeUpdate(request);
	}
	catch (IOException e)
	{
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }

    public void stop()
    {
	try
	{
	    statement.close();
	    connection.close();
	}
	catch (SQLException e)
	{
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public void addAnime(Anime anime) throws SQLException
    {
	PreparedStatement statement = connection.prepareStatement(
		"INSERT INTO Anime VALUES (?,?,?,?)",
		Statement.RETURN_GENERATED_KEYS);

	statement.setString(2, anime.getName());
	statement.setString(3, anime.getLocation().getAbsolutePath());
	statement.setString(4, anime.getGenre());
	statement.executeUpdate();

	try (ResultSet generatedKeys = statement.getGeneratedKeys())
	{
	    if (generatedKeys.next())
	    {
		anime.setID(generatedKeys.getInt(1));
	    }
	    else
	    {
		throw new SQLException("Creating user failed, no ID obtained.");
	    }
	}
    }

    public void addEpisode(AnimeEntry entry) throws SQLException
    {
	PreparedStatement statement = connection
		.prepareStatement("INSERT INTO Episode VALUES (?,?,?,?,?,?,?)");

	statement.setString(2, entry.getName());
	statement.setDouble(3, entry.getEpisode());
	statement.setString(4, entry.getTeam());
	statement.setString(5, entry.getFile().getAbsolutePath());
	statement.setInt(6, entry.getSeason());
	statement.setInt(7, entry.getAnimeID());
	statement.executeUpdate();
    }

    public AnimeInfo getAnimeName(AnimeEntry episode) throws SQLException
    {
	String request = "SELECT * FROM Lookup_Anime WHERE episode_name = \""
		+ episode.getName() + "\"";
	ResultSet set = statement.executeQuery(request);
	if (set.isAfterLast())
	    return new AnimeInfo();
	AnimeInfo info = new AnimeInfo(set.getInt("anime_id"),
		set.getInt("season"), set.getInt("id"));
	return info;
    }

    public Anime getAnime(int animeId) throws SQLException
    {
	Anime anime = new Anime();
	String request = "SELECT * FROM Anime WHERE id = \"" + animeId + "\"";
	ResultSet set = statement.executeQuery(request);
	if (set.isAfterLast())
	    return null;
	anime.setName(set.getString("name"));
	anime.setLocation(new File(set.getString("location")));
	anime.setID(set.getInt("id"));
	return anime;
    }

    public ArrayList<Anime> getAnimeList()
    {
	ArrayList<Anime> animes = new ArrayList<Anime>();
	ResultSet result;
	try
	{
	    result = statement.executeQuery("SELECT * FROM Anime");
	    while (result.next())
	    {
		Anime next = new Anime();
		next.setName(result.getString("name"));
		next.setLocation(new File(result.getString("location")));
		animes.add(next);
	    }
	}
	catch (SQLException e)
	{
	    e.printStackTrace();
	}
	return animes;
    }

    public void erase() throws SQLException
    {
	statement.executeUpdate("DELETE FROM Episode");
	statement.executeUpdate("DELETE FROM Anime");
	statement.executeUpdate("DELETE FROM Lookup_Anime");
    }

    public AnimeInfo createLookup(int animeId, String EpisodeName, int season)
	    throws SQLException
    {
	int id = -1;
	PreparedStatement statement = connection.prepareStatement(
		"INSERT INTO Lookup_Anime VALUES (?,?,?,?)",
		Statement.RETURN_GENERATED_KEYS);
	statement.setInt(2, animeId);
	statement.setString(3, EpisodeName);
	statement.setInt(4, season);
	statement.executeUpdate();

	try (ResultSet generatedKeys = statement.getGeneratedKeys())
	{
	    if (generatedKeys.next())
	    {
		id = generatedKeys.getInt(1);
	    }
	    else
	    {
		throw new SQLException("Creating user failed, no ID obtained.");
	    }
	}

	return new AnimeInfo(animeId, season, id);
    }

    public ArrayList<Tag> getTags(int animeId)
    {
	ArrayList<Tag> tags = new ArrayList<Tag>();
	try
	{
	    String request = "SELECT t.id, t.label FROM Tag t, Tag_Lookup tl, Anime a "
		    + "WHERE a.id = \""
		    + animeId
		    + "\" AND a.id = tl.anime_id AND tl.anime_id = t.id";
	    ResultSet set = statement.executeQuery(request);
	    while (set.next())
	    {
		Tag next = new Tag();
		next.setId(set.getInt("id"));
		next.setLabel(set.getString("label"));
		tags.add(next);
	    }
	}
	catch (SQLException e)
	{
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	return tags;
    }

    public void addTag(Tag tag) throws SQLException
    {
	PreparedStatement statement = connection.prepareStatement(
		"INSERT INTO Tag VALUES (?,?)",
		Statement.RETURN_GENERATED_KEYS);

	statement.setString(2, tag.getLabel());
	statement.executeUpdate();

	try (ResultSet generatedKeys = statement.getGeneratedKeys())
	{
	    if (generatedKeys.next())
	    {
		tag.setId(generatedKeys.getInt(1));
	    }
	    else
	    {
		throw new SQLException("Creating tag failed, no ID obtained.");
	    }
	}
    }
}
