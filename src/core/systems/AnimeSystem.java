package core.systems;

import java.sql.SQLException;
import java.util.ArrayList;
import datastructure.*;

public class AnimeSystem
{
    private AnimeBase db = null;

    public AnimeSystem(AnimeBase db)
    {
	this.db = db;
    }

    public boolean classifyEpisode(AnimeEntry episode) throws SQLException
    {
	AnimeInfo animeInfo;
	animeInfo = db.getAnimeName(episode);
	if (!animeInfo.isValid())
	    return false;
	Anime anime = db.getAnime(animeInfo.getAnimeId());
	if (anime == null)
	    return false;
	this.updateAnime(episode, animeInfo, anime);
	db.addEpisode(episode);
	return true;
    }

    private void updateAnime(AnimeEntry episode, AnimeInfo animeInfo,
	    Anime anime)
    {
	episode.setSeason(animeInfo.getSeason());
	episode.setAnimeID(anime.getID());
    }

    public void addAnime(Anime anime, AnimeEntry episode,
	    ArrayList<AnimeEntry> entries)
    {
	AnimeInfo info = null;
	try
	{
	    this.db.addAnime(anime);
	    info = this.db.createLookup(anime.getID(), episode.getName(),
		    episode.getSeason());
	}
	catch (SQLException e)
	{
	    e.printStackTrace();
	    return;
	}
	String episodesName = episode.getName();
	ArrayList<AnimeEntry> resolved = new ArrayList<AnimeEntry>();
	for (AnimeEntry episodeToTreat : entries)
	{
	    if (episodeToTreat.getName().equalsIgnoreCase(episodesName))
	    {
		resolved.add(episodeToTreat);
		this.updateAnime(episodeToTreat, info, anime);
		System.out.println("episode : " + episodeToTreat
			+ " id anime : " + anime.getID()
			+ " episode info id : " + episodeToTreat.getAnimeID());
		try
		{
		    db.addEpisode(episodeToTreat);
		}
		catch (SQLException e)
		{
		    e.printStackTrace();
		}
	    }
	}
	entries.removeAll(resolved);
    }
}
