package datastructure;

public class Tag
{
    private String label;
    private int    id;

    public Tag(String label)
    {
	this.label = label;
    }

    public Tag()
    {

    }

    public String getLabel()
    {
	return label;
    }

    public void setLabel(String label)
    {
	this.label = label;
    }

    public int getId()
    {
	return id;
    }

    public void setId(int id)
    {
	this.id = id;
    }
}
