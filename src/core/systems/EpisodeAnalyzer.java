package core.systems;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import tools.Parser;
import datastructure.AnimeEntry;

public class EpisodeAnalyzer {
	public AnimeEntry parse(File entry)
	{
		AnimeEntry episode = new AnimeEntry();
		episode.setFile(entry);
		Parser parser = new Parser(new ByteArrayInputStream(entry.getName().getBytes()));
		episode.setTeam(this.parseTeam(parser));
		episode.setName(this.readName(parser));
		this.readEpisodeNumber(parser, episode);
		return episode;
	}

	private void readEpisodeNumber(Parser parser, AnimeEntry episode) {
		try {
			while (!parser.peekRange('0', '9') && !parser.peekText("Ep") && !parser.peekText("ep"))
			{
				if (parser.readChar('.') || parser.readChar('['))
					return;
				parser.skip();
			}
			parser.readText("ep");
			parser.readText("Ep");
			while(parser.readBlank());
			parser.beginCapture("episode");
			while (parser.readRange('0', '9') || parser.readChar('.'));
			String episodeNum = parser.endCapture("episode");
			episode.setEpisode(Double.parseDouble(episodeNum));
			if (parser.readChar('v'))
			{
				parser.beginCapture("version");
				while (parser.readRange('0', '9') || parser.readChar('.'));
				String versionNum = parser.endCapture("version");
				episode.setVersion(Double.parseDouble(versionNum));
		
			}
		} catch (IOException e) {
			return;
		}
	}

	private String readName(Parser parser) {
		try {
			while (parser.readBlank() || parser.readChar('_'));
			parser.beginCapture("name");
			while (parser.readChar('-')
					||(parser.readBlank() && !parser.peekChar('-'))
					|| (parser.readChar('_') && !parser.peekChar('-'))
					|| parser.readCharacter()
					|| parser.readChar('~'));
		} catch (IOException e) {
			return "";
		}
		String name = parser.endCapture("name");
		return name.replaceAll("_", " ");
	}

	private String parseTeam(Parser parser){
		try {
			if (parser.readChar('['))
			{
				parser.beginCapture("team");
				if (parser.readUntil(']'))
				{
					String team = parser.endCapture("team");
					parser.readChar(']');
					return team;
				}
				return "---";
			}
		} catch (IOException e) {
			return "---";
		}
		return "---";
	}
}
