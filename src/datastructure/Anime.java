package datastructure;
import java.io.File;

public class Anime {
	private String name;
	private File location;
	private int id;
	private String genre;
	
	public Anime(String name, String genre, String location)
	{
		this.name = name;
		this.genre = genre;
		this.location = new File(location);
	}
	public Anime()
	{
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setGenre(String genre)
	{
		this.genre = genre;
	}
	public void setID(int id)
	{
		this.id = id;
	}
	public File getLocation() {
		return location;
	}
	public void setLocation(File location) {
		this.location = location;
	}
	public int getID() {
		// TODO Auto-generated method stub
		return this.id;
	}
	public String getGenre() {
		// TODO Auto-generated method stub
		return this.genre;
	}
	
	@Override
	public String toString()
	{
		return this.name;
	}
}
