package view;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import tools.UpdateMessage;
import controller.Controller;
import controller.Observer;

public class View extends JFrame implements Observer {

	private static final long serialVersionUID = 1L;
	private Controller controller;
	private JTabbedPane tabs;
	private UnclassifiedForm		unclassifiedForm;

	public View(String s, Controller controller) {
		super(s);

		this.controller = controller;
		this.controller.addObserver(this);

		setSize(800, 600);
		setResizable(true);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.unclassifiedForm = new UnclassifiedForm(controller);
		tabs = new JTabbedPane(SwingConstants.TOP);

		this.tabs.addTab("Unclassified", null, this.unclassifiedForm);
		this.add(this.tabs);
		this.setJMenuBar(new Menu());
		setVisible(true);
	}


	public void update(UpdateMessage message) {
		switch (message.getState()) {
		case INIT:
		case NEWANIME:
			break;
		case SCAN:
			break;
		}
		repaint();
	}

}
