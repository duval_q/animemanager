package view;


import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JCheckBox;

import datastructure.Tag;

public class TagItem extends JCheckBox
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Tag tag;
    private boolean isSelected = false;
    
    public TagItem(Tag tag)
    {
	super(tag.getLabel());
	this.tag = tag;
	
	addMouseListener(new MouseAdapter()
	{
	    public void mouseClicked(MouseEvent event)
	    {
		//item.setSelected(! item.isSelected());
	    }
	});
    }

    public boolean isSelected()
    {
        return isSelected;
    }

    public void setSelected(boolean isSelected)
    {
        this.isSelected = isSelected;
    }
    
    public String toString()
    {
	return tag.getLabel();
    }
}
