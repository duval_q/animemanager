package tools;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class Parser {
	PeekableInputStream stream;
	HashMap<String, StringBuffer>	capture;
	
	public Parser(InputStream stream)
	{
		this.capture = new HashMap<String, StringBuffer>();
		this.stream = new PeekableInputStream(stream);
	}
	
	public boolean peekChar(char c) throws IOException
	{
		return this.peekChar(c, 0);
	}
	
	private boolean peekChar(char c, int idx) throws IOException
	{
		char res;
		res = (char)this.stream.peek(idx);
		return (res == c);
	}
	
	public boolean readChar(char c) throws IOException
	{
		if (this.peekChar(c))
		{
			this.stream.read();
			this.accept(c);
			return true;
		}
		return false;
	}
	
	public boolean peekRange(char begin, char end) throws IOException
	{
		char current;
			current = (char)this.stream.peek();
		return (current >= begin && current <= end);
	}
	
	public boolean readRange(char begin, char end) throws IOException
	{
		if (this.peekRange(begin, end))
		{
			char value;
			value = (char)this.stream.read();
			this.accept(value);
			return true;
		}
		return false;
	}
	
	public boolean peekIdentifier() throws IOException
	{
		return this.peekRange('a', 'z') || this.peekRange('A', 'Z') || this.peekRange('0', '9') || this.peekChar('_');
	}
	
	public boolean readUntil(char c) throws IOException
	{
		byte[] data;
		int idx = 0;
		while (!this.peekChar(c, idx))
		{
			idx++;
		}
		data = new byte[idx];
		this.stream.read(data);
		this.accept(data);
		return true;
	}

	public boolean readInteger() throws IOException
	{
		return this.readRange('0', '9');
	}
	
	public boolean readIdentifier() throws IOException
	{
		return this.readCharacter() || this.readRange('0', '9') || this.readChar('_');
	}
	
	public boolean readText(String text) throws IOException
	{
		int size = text.length();
		for (int i = 0; i < size; i++)
		{
			try {
				if (text.getBytes()[i] != (char)this.stream.peek(i))
				{
					return false;
				}
			} catch (IOException e) {
				return false;
			}
		}
		this.stream.skip(size);
		this.accept(text);
		return true;
	}
	
	public void beginCapture(String tag)
	{
		this.capture.put(tag, new StringBuffer());
	}
	
	public String endCapture(String tag)
	{
		return this.capture.remove(tag).toString();
	}
	
	private void accept(String text)
	{
		for (Map.Entry<String, StringBuffer> entry : this.capture.entrySet())
		{
			entry.getValue().append(text);
		}
	}
	
	private void accept(char c)
	{
		for (Map.Entry<String, StringBuffer> entry : this.capture.entrySet())
		{
			entry.getValue().append(c);
		}
	}
	
	
	private void accept(byte[] data) {
		for (Map.Entry<String, StringBuffer> entry : this.capture.entrySet())
		{
			String s = new String(data);
			entry.getValue().append(s);
		}
	}

	public boolean readBlank() throws IOException {
		return this.readChar(' ') || this.readChar('\t') || this.readChar('\r') || this.readChar('\n');
	}

	public boolean readCharacter() throws IOException {
		return this.readRange('a', 'z') || this.readRange('A', 'Z');
	}

	public boolean peekText(String text) {
		int size = text.length();
		for (int i = 0; i < size; i++)
		{
			try {
				if (text.getBytes()[i] != (char)this.stream.peek(i))
				{
					return false;
				}
			} catch (IOException e) {
				return false;
			}
		}
		return true;
	}

	public long skip() throws IOException {
		return this.stream.skip(1);
	}
	
	public long skip(long size) throws IOException
	{
		return this.stream.skip(size);
	}
}
