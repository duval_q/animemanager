package datastructure;

public class AnimeInfo {
	private int 	animeId;
	private int		season;
	private int		id;
	
	public AnimeInfo(int animeId, int season, int id)
	{
		this.animeId = animeId;
		this.season = season;
		this.id = id;
	}
	
	public AnimeInfo() {
		animeId = -1;
		season = -1;
	}

	public boolean isValid()
	{
		return animeId > 0;
	}

	public int getAnimeId() {
		return animeId;
	}

	public int getSeason() {
		return season;
	}

	public int getID() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
