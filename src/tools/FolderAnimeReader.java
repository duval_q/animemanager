package tools;

import java.io.File;
import java.util.ArrayList;

import core.systems.EpisodeAnalyzer;
import datastructure.AnimeEntry;

public class FolderAnimeReader {
	
	public ArrayList<AnimeEntry> listFilesForFolder(final File folder) {
		ArrayList<AnimeEntry> entries = new ArrayList<AnimeEntry>();
		EpisodeAnalyzer analyzer = new EpisodeAnalyzer();
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	        	//entries.addAll(listFilesForFolder(fileEntry));
	        } else {
	            AnimeEntry next = analyzer.parse(fileEntry);
	            entries.add(next);
	        }
	    }
		return entries;
	}
}
