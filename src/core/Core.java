package core;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;

import controller.Controller;
import model.AbstractModel;
import datastructure.*;
import core.systems.AnimeBase;
import core.systems.AnimeSystem;
import tools.FolderAnimeReader;
import tools.UpdateMessage.UpdateState;
import view.View;

public class Core extends AbstractModel{



	private ArrayList<AnimeEntry> unresolved;
	
	
	private AnimeBase db;
	private AnimeSystem system;
	private FolderAnimeReader reader;
	
	public Core(){
		unresolved = new ArrayList<AnimeEntry>();
		this.db = new AnimeBase();
		try {
			db.initialise("jdbc:sqlite:Anime");
			db.erase();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.system = new AnimeSystem(db);
		this.reader =  new FolderAnimeReader();
	}
	

	private void initialize() {
		updateObserver("Core initialized", UpdateState.INIT);
	}

	public void scan() {
		unresolved.clear();
		ArrayList<AnimeEntry> list = reader.listFilesForFolder(new File("//BEHEMOTH/anime/a trier"));
		for (AnimeEntry episode : list)
		{
			if (episode.isValid())
			{
				try {
					if (system.classifyEpisode(episode))
					{
						System.out.println("registered " + episode.getName());
					}
					else
						this.unresolved.add(episode);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		updateObserver("scan OK", UpdateState.SCAN);
	}
	
	public void registerAnime(Anime anime, AnimeEntry episode)
	{
		this.system.addAnime(anime, episode, unresolved);
		updateObserver("update anime", UpdateState.SCAN);
		updateObserver("update anime", UpdateState.NEWANIME);
	}

	public ArrayList<AnimeEntry> getListAnimeUnresolved() {
		
		return this.unresolved;
	}
	

	public ArrayList<Anime> getListAnimeKnown() {
		// TODO Auto-generated method stub
		return this.db.getAnimeList();
	}
	
	public static void main(String[] args) {
		Core core = new Core();
		Controller controller = new Controller(core);
		@SuppressWarnings("unused")
		View view = new View("AnimeScan", controller);
		core.initialize();
	}
}
