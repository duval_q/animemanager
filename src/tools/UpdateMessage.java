package tools;

public class UpdateMessage {
	
	public enum UpdateState {
		INIT,
		SCAN, NEWANIME
	}
	
	private String message;
	private UpdateState state;
	
	public UpdateMessage(String message, UpdateState state) {
		this.state = state;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public UpdateState getState() {
		return state;
	}

	public void setState(UpdateState state) {
		this.state = state;
	}
}
