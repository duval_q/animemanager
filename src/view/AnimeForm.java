package view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import datastructure.Anime;
import datastructure.AnimeEntry;

public class AnimeForm extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 494075365450181592L;
	private JTextField txtfldLocation;
	private AnimeEntry entry = null;
	private JLabel		currentAnimeLbl;
	private JButton		accept;
	
	JComboBox<String> cboxName;
	JComboBox<String> cboxGenre;
	private JFormattedTextField ftfSeason;
	
	public AnimeForm(ActionListener acceptListener)
	{
		accept = new JButton("Accept");
		accept.addActionListener(acceptListener);
		this.currentAnimeLbl = new JLabel("---");
		
	}
	
	public void updateInfos(ArrayList<Anime> listAnimes)
	{
		this.removeAll();
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		// add label anime
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1;
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 0, 10, 0); // top padding
		gbc.anchor = GridBagConstraints.FIRST_LINE_START;

		this.add(currentAnimeLbl, gbc);
		
		// add label anime
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 0, 10, 0); // top padding
		gbc.anchor = GridBagConstraints.FIRST_LINE_START;

		this.add(new JLabel("ANIME"), gbc);

		// add label name
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 0.5;
		gbc.gridx = 0;
		gbc.gridy = 1;
		// gbc.weighty = 0;
		gbc.insets = new Insets(12, 0, 0, 10); // top padding
		this.add(new JLabel("Nom"), gbc);

		// add combobox name
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.5;
		gbc.gridx = 1;
		gbc.gridy = 1;
		// gbc.weighty = 0;
		gbc.insets = new Insets(10, 0, 0, 10); // top padding

		cboxName = new JComboBox<String>();
		cboxName.setEditable(true);
		for (Anime anime : listAnimes) {
			cboxName.addItem(anime.getName());
		}
		this.add(cboxName, gbc);
		
		
		// add label season
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 0.5;
		gbc.gridx = 0;
		gbc.gridy = 2;
		// gbc.weighty = 0;
		gbc.insets = new Insets(12, 0, 0, 10); // top padding
		this.add(new JLabel("Saison"), gbc);
		
		// add textfield season
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.5;
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.ipady = 8;
		gbc.insets = new Insets(10, 0, 0, 10); // top padding
		try
		{
			MaskFormatter mask = new MaskFormatter("#"); // format accept only 1 numbers
			ftfSeason = new JFormattedTextField(mask);
			this.add(ftfSeason, gbc);
		}
		catch (ParseException e1)
		{
		}
		

		// add label genre
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 0.5;
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.insets = new Insets(12, 0, 0, 10); // top padding
		this.add(new JLabel("Genre"), gbc);

		// add combobox genre
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.5;
		gbc.gridx = 1;
		gbc.gridy = 3;
		gbc.insets = new Insets(10, 0, 0, 10); // top padding

		cboxGenre = new JComboBox<String>();
		cboxGenre.setEditable(true);
		// TODO r�cup�rer la liste des genres
		//for (Anime anime : listAnimes) {
			// cboxGenre.addItem(anime);
		//}
		this.add(cboxGenre, gbc);

		
		// add label location
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 0.5;
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.insets = new Insets(12, 0, 0, 10); // top padding
		gbc.weighty = 1;
		this.add(new JLabel("Localisation"), gbc);

		// add textField location and filechooser
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.5;
		gbc.gridx = 1;
		gbc.gridy = 4;
		gbc.ipady = 8;
		gbc.insets = new Insets(10, 0, 0, 10); // top padding

		txtfldLocation = new JTextField();
		txtfldLocation.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFileChooser fChooser = new JFileChooser();
			    fChooser.setCurrentDirectory(new java.io.File(".")); // on changera �a plus tard
			    fChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				fChooser.setAcceptAllFileFilterUsed(false);
				if (fChooser.showOpenDialog(AnimeForm.this) == JFileChooser.APPROVE_OPTION)
				{
					txtfldLocation.setText(fChooser.getSelectedFile().getPath());
				}
			}
		});
		this.add(txtfldLocation, gbc);
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 0.5;
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.insets = new Insets(12, 0, 0, 10); // top padding
		gbc.weighty = 1;
		this.add(accept, gbc);
	}
	public void setAnime(AnimeEntry entry) {
		this.entry  = entry;
		this.currentAnimeLbl.setText(this.entry.toString());
		this.repaint();
	}

	public AnimeEntry getAnimeEntry() {
		// TODO Auto-generated method stub
		return this.entry;
	}

	public String getGenre() {
		// TODO Auto-generated method stub
		return (String)this.cboxGenre.getSelectedItem();
	}

	public String getFolderLocation() {
		// TODO Auto-generated method stub
		return txtfldLocation.getText();
	}

	public String getAnime() {
		// TODO Auto-generated method stub
		return (String)this.cboxName.getSelectedItem();
	}
	
	public int getSeason() {
		// TODO Auto-generated method stub
		return Integer.parseInt(this.ftfSeason.getText());
	}
}
