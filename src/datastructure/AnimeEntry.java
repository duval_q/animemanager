package datastructure;

import java.io.File;

public class AnimeEntry {
	
	private String	name;
	private String	team;
	private double	episode;
	private double	version;
	private int		season;
	private int 	animeID;
	private File	file;


	public AnimeEntry()
	{ 
		this.episode = -1;
		this.season = -1;
		this.animeID = -1;
		this.version = 1;
	}
	
	public void setName(String name) {
		this.name = name;
	}



	public void setTeam(String team) {
		this.team = team;
	}



	public void setEpisode(double episode) {
		this.episode = episode;
	}



	public void setVersion(double version) {
		this.version = version;
	}



	public void setFile(File file) {
		this.file = file;
	}



	public double getVersion() {
		return version;
	}

	public int getSeason() {
		return season;
	}

	public boolean isValid() {
		return !name.isEmpty() && episode != -1;
	}

	public String getName() {
		return name;
	}

	public String getTeam() {
		return team;
	}

	public double getEpisode() {
		return episode;
	}
	
	public File getFile()
	{
		return this.file;
	}

	public void setSeason(int season) {
		this.season = season;
	}
	
	@Override
	public String toString()
	{
		return this.name + " " + this.episode;
	}

	public void setAnimeID(int id) {
		this.animeID = id;
	}



	public int getAnimeID() {
		return this.animeID;
	}

	public void appendToName(String string) {
		this.name = this.name + string;
	}
}
