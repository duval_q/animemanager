-- -----------------------------------------------------
-- Table Anime
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Anime (
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,
  name TEXT NOT NULL,
  location TEXT NOT NULL,
  genre TEXT NULL);


-- -----------------------------------------------------
-- Table Episode
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Episode (
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  name TEXT NOT NULL,
  number REAL NOT NULL,
  team TEXT NULL,
  path TEXT NOT NULL,
  season INTEGER NULL,
  Anime_id INTEGER NOT NULL,
  FOREIGN KEY (Anime_id)
    REFERENCES Anime (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table Lookup_Anime
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Lookup_Anime (
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  Anime_id INTEGER NOT NULL,
  episode_name VARCHAR(45) NOT NULL,
  season INTEGER NOT NULL,
  FOREIGN KEY (Anime_id)
    REFERENCES Anime (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table Tag
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Tag (
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  label TEXT NOT NULL);


-- -----------------------------------------------------
-- Table Tag_Lookup
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Tag_Lookup (
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  Anime_id INTEGER NOT NULL,
  Tag_id INTEGER NOT NULL,
  FOREIGN KEY (Anime_id)
    REFERENCES Anime (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  FOREIGN KEY (Tag_id)
    REFERENCES Tag (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE);