package view;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionListener;
import datastructure.AnimeEntry;

public class ListPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	JList<AnimeEntry> list;
	DefaultListModel<AnimeEntry> model;

	public ListPanel(ListSelectionListener select) {
		setLayout(new BorderLayout());
		model = new DefaultListModel<AnimeEntry>();
		list = new JList<AnimeEntry>(model);
		JScrollPane pane = new JScrollPane(list);
		
		list.addListSelectionListener(/**/select);
		add(pane, BorderLayout.CENTER);
	}
	
	public AnimeEntry getSelected()
	{
		return this.list.getSelectedValue();
	}
	
	public void addElements(ArrayList<AnimeEntry> list)
	{
		model.removeAllElements();
		for(AnimeEntry item : list)
		{
			model.addElement(item);
		}
	}
}
