package controller;

import java.util.ArrayList;

import datastructure.Anime;
import datastructure.AnimeEntry;
import core.Core;


public class Controller {
	private Core core;

	public Controller(Core core){
		this.core = core;
	}
	
	/**
	 * permet aux vues de s'abonner au modele pour les mises a jour
	 * @param obs
	 * 				une classe qui implemente Observer
	 * @see pattern_observer#Observer
	 */
	public void addObserver(Observer obs){
		core.addObserver(obs);
	}
	
	/**
	 * force la mise a jour des vues
	 */
	public void updateForce(){
		//core.updateObserver();
	}

	public void scan() {
		core.scan();	
	}

	public ArrayList<AnimeEntry> getListAnimeUnresolved() {
		
		return core.getListAnimeUnresolved();
	}

	public ArrayList<Anime> getListAnimeKnown() {
		// TODO Auto-generated method stub
		return core.getListAnimeKnown();
	}

	public void registerAnime(AnimeEntry episode, Anime anime) {
		this.core.registerAnime(anime, episode);
	}
}
