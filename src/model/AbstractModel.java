package model;

import java.util.ArrayList;

import controller.Observer;

import tools.UpdateMessage;
import tools.UpdateMessage.UpdateState;

public abstract class AbstractModel{
	
	private ArrayList<Observer> listObserver = new ArrayList<Observer>();

	
	public AbstractModel() {

	}
	
	public void addObserver(Observer obs) {
		System.out.println("ajout d'un observer : " + obs.getClass());
		this.listObserver.add(obs);
	}

	public void removeObserver() {
		listObserver = new ArrayList<Observer>();
	}
	
	public void updateObserver(String message, UpdateState state) {
		for(Observer obs : this.listObserver )
		{
			obs.update(new UpdateMessage(message, state));
		}
	}
}
